import { PersonInfo } from "./PersonInfo";

export function Grupo({ datos, id }) {
  return (
    <div className="row my-5">
      <h4>Grupo #{id + 1}</h4>
      {datos
        ? datos.map((data, index) => {
            return (
              <PersonInfo
                key={index}
                Nombres={data.Nombres}
                Cargo={data.Cargo}
                Foto={data.Foto}
              />
            );
          })
        : null}
    </div>
  );
}
