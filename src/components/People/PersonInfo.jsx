import images from "../../assets/img/images";

export function PersonInfo({ Nombres, Foto, Cargo }) {
  if (!Foto) Foto = "assets/img/default-foto.jpg";
  return (
    <div className="col-2 m-3">
      <div className="card" style={{ width: "10rem" }}>
        <img
          src={images[Foto]}
          className="card-img-top "
          style={{
            borderRadius: "50%",
            height: "10rem",
            objectFit: "cover",
          }}
          alt="Loading..."
        />
        <div className="card-body">
          <h5 className="card-title">{Nombres}</h5>
          <p>{Cargo}</p>
        </div>
      </div>
    </div>
  );
}
