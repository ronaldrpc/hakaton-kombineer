export const ReadJsonFile = ({ data, setData }) => {
  let jsonList = [];

  const onChange = (e) => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = (e) => {
      let jsonData = JSON.parse(e.target.result);
      for (let ele in jsonData) {
        jsonList.push(jsonData[ele]);
      }
      console.log(jsonList);
      setData([...jsonList]);
    };
  };

  return (
    <div className="container ">
      <div className="m-5">
        <h4>JSON Selector</h4>
        <input type="file" onChange={onChange} />
      </div>
    </div>
  );
};
