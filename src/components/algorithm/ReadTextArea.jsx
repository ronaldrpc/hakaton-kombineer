import { useState } from "react";

export const ReadTextArea = ({ data, setData }) => {
  const [value, setValue] = useState(null);
  let jsonList = [];

  function handleChange(e) {
    let textA = document.getElementById("textarea");
    let jsonData = JSON.parse(textA.value);
    for (let ele in jsonData) {
      jsonList.push(jsonData[ele]);
    }
    console.log(jsonList);
    setData([...jsonList]);
  }

  return (
    <div className="container">
      <div className="m-5">
        <h4>Text Selector</h4>
        <form>
          <label>
            <textarea id="textarea" rows="10" cols="35" />
          </label>
          <div>
            <button type="button" onClick={handleChange}>
              Load
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
