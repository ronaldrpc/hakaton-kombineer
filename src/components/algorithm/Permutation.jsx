import { useEffect } from "react";
import { ReadXlsxFile } from "./ReadXlsxFile";

export const Permutation = ({ data, setGroups, amount }) => {
  let nameList = [];
  let groups = {};

  const shuffleArray = (array) => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  };

  function divideList(amount) {
    let lista = [...nameList];
    shuffleArray(lista);
    let indexGroup = 0;
    for (let i = 0; i < lista.length; i += amount) {
      let temp = lista.slice(i, i + amount);
      if (temp.length < amount) groups[indexGroup - 1].push(...temp);
      else groups[indexGroup] = temp;
      indexGroup++;
    }

    let jsonList = [];
    for (let ele in groups) {
      jsonList.push(groups[ele]);
    }
    setGroups([...jsonList]);
  }

  function recorrerData(amount) {
    for (let element of data) {
      nameList.push(element);
    }
    divideList(amount);
  }

  useEffect(() => {
    recorrerData(amount);
  }, [amount]);

  return <div> {} </div>;
};
