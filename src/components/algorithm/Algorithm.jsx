import { useState } from "react";
import { Permutation } from "./Permutation";
import { ReadJsonFile } from "./ReadJsonFile";
import { ReadTextArea } from "./ReadTextArea";
import { ReadXlsxFile } from "./ReadXlsxFile";

const Algorithm = ({ groups, setGroups, amount }) => {
  const [data, setData] = useState(null);

  return (
    <div>
      <div className="archivos">
        <ReadXlsxFile data={data} setData={setData} />
        <ReadJsonFile data={data} setData={setData} />
        <ReadTextArea data={data} setData={setData} />
        {data ? (
          <Permutation data={data} setGroups={setGroups} amount={amount} />
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default Algorithm;
