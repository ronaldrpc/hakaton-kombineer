import { useState } from "react";
import * as XLSX from "xlsx";
// import ok from "../assets/CAR IV.xlsx";

export const ReadXlsxFile = ({ data, setData }) => {
  let datos = null;

  const onChange = (e) => {
    const [file] = e.target.files;
    const reader = new FileReader();

    reader.onload = (evt) => {
      const bstr = evt.target.result;
      const wb = XLSX.read(bstr, { type: "binary" });
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      datos = XLSX.utils.sheet_to_json(ws, {});
      console.log(datos);
      setData([...datos]);
    };
    reader.readAsBinaryString(file);
  };

  return (
    <div className="container">
      <div className="m-5">
        <h4>XLSX Selector</h4>
        <input type="file" onChange={onChange} />
      </div>
    </div>
  );
};
