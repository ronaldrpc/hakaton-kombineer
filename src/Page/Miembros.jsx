import erick from "../../assets/img/lideresproyecto/Erick-C.jpeg";
import leandro from "../../assets/img/lideresproyecto/Leandro-M.jpeg";
import zuleydis from "../../assets/img/lideresproyecto/Zuleydis-B.jpeg";

function Card({ title, img, text, offset }) {
  const clase = offset ? "offset-sm-3" : "";
  return (
    <div className={`col-sm-6 ${clase}`}>
      <div className="card mb-3">
        <div className="row g-0">
          <div className="col-md-4">
            <img style={{ height: "220px", objectFit: "cover" }} src={img} />
          </div>
          <div className="col-md-8 d-grid align-content-center">
            <div className="card-body">
              <h4 className="card-title text-center">{title}</h4>
              <p className="card-text">{text}</p>
              <p className="card-text" style={{ textAlign: "right" }}>
                <small className="text-muted">#SomosCarIV</small>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export function LideresProyecto() {
  return (
    <div>
      <h1 className="card-title text-center fs-1 py-5">LIDERES DE PROYECTO</h1>
      <h1 className="card-title text-center fs-2 ">PROJECT MANAGER</h1>

      <div className="container">
        <div className="row">
          <Card
            offset={true}
            title="Erick Contreras Barrios"
            text="Tengo 22 años, de Cartagena Colombia y soy apasionado por
                      el desarrollo y los deportes."
            img={erick}
          />
        </div>
      </div>

      <h1 className="card-title text-center fs-2 py-3">SCRUM MASTER</h1>
      <div className="container">
        <div className="row">
          <Card
            title="Zuleydis Barrios Peña"
            text="Ingeniera en sistema, actualmente en formación en lsv CAR IV,
                apasionada por el frontend y el aprendizaje continuo."
            img={zuleydis}
          />

          <Card
            title="Leandro Meza Vasquez"
            text="Ingeniero de sistemas con el sueño de tener su propia
                      empresa, que ha aprendido que Dios es bueno todo el
                      tiempo, sin temor a los retos y vivir la visa sabroso."
            img={leandro}
          />
        </div>
      </div>
    </div>
  );
}
