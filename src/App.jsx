import { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Header } from "./components/Nav/Header";
import { Routes, Route } from "react-router-dom";
import { Footer } from "./components/Footer/Footer";
import { Grupo } from "./components/People/Grupo";
import { Permutation } from "./components/algorithm/Permutation";
import { ReadJsonFile } from "./components/algorithm/ReadJsonFile";
import { ReadTextArea } from "./components/algorithm/ReadTextArea";
import { ReadXlsxFile } from "./components/algorithm/ReadXlsxFile";
import Algorithm from "./components/algorithm/Algorithm";

function App() {
  const [data, setData] = useState(null);
  const [groups, setGroups] = useState(null);
  const [integrantes, setIntegrantes] = useState(1);

  function onChangeInput(e) {
    const cantidad = parseInt(e.target.value);
    setIntegrantes(cantidad);
  }

  return (
    <div className="App ">
      <Header />
      <Routes>
        <Route>{/* <Route path="*" element={<App />} /> */}</Route>
      </Routes>
      <div className="container">
        <h1 className="text-center py-3 mb-0">Hakaton Kombineer</h1>
        <div className="d-flex py-3">
          <div className="input-group m-3" style={{ width: "30%" }}>
            <span className="input-group-text" id="addon-wrapping">
              Numero de integrantes
            </span>
            <input
              id="amount"
              type="number"
              className="form-control"
              placeholder=""
              aria-label="Example text with button addon"
              aria-describedby="button-addon1"
              onChange={onChangeInput}
              value={integrantes}
            />
          </div>
        </div>
        <Algorithm groups={groups} setGroups={setGroups} amount={integrantes} />
        {/* <div className="archivos mb-3">
          <ReadXlsxFile data={data} setData={setData} />
          <ReadJsonFile data={data} setData={setData} />
          <ReadTextArea data={data} setData={setData} />
          {data ? (
            <Permutation
              data={data}
              setGroups={setGroups}
              amount={integrantes}
            />
          ) : (
            ""
          )}
        </div> */}

        {groups
          ? groups.map((_list, index) => {
              return <Grupo key={index} datos={_list} id={index} />;
            })
          : "Loading..."}
      </div>
      <Footer />
    </div>
  );
}

export default App;
