[
    {
        "Nombres": "Alan de Jesus Sanchez Tovar",
        "Foto": "assets/img/alan-sanchez.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Alvaro Jose Vergara Garcia",
        "Foto": "assets/img/alvaro-vergara.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Amanda Marcela Quintana Julio",
        "Foto": "assets/img/amanda-quintana.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Andrés Felipe Molinares Bolaños",
        "Foto": "assets/img/andres-molinares.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Andres Fernando Garcia Meza",
        "Foto": "assets/img/andres-garcia.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Angélica Patricia Morales Daza",
        "Foto": "assets/img/angelica-morales.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Danith Ester Babilonia Meza",
        "Foto": "assets/img/danith-babilonia.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Deiver Jose Vazquez Moreno",
        "Foto": "assets/img/deiver-vazquez.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Erick De Jesus Contreras Barrios",
        "Foto": "assets/img/erick-contreras.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Erik Manuel Herazo Jimenez",
        "Foto": "assets/img/erick-herazo.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Ever Josue Navarro Padilla",
        "Foto": "assets/img/ever-navarro.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Herly de Jesus Castillo Castillo",
        "Foto": "assets/img/herly-castillo.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Isaac Daniel Benavides Torres",
        "Foto": "assets/img/isaac-benavides.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Jair Calderón Velaides",
        "Foto": "assets/img/jair-calderon.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Jhonatan Maunuel Alvarez De la hoz",
        "Foto": "assets/img/jhonatan-alvarez.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Jorge Luis Zetien Luna",
        "Foto": "assets/img/jorge-zetien.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Juan José Payares Trocha",
        "Foto": "assets/img/juan-payares.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Juan Pablo Lara Angulo",
        "Foto": "assets/img/juan-lara.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Juan Sebastian Sierra Bravo",
        "Foto": "assets/img/juan-sierra.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Leandro Meza Vasquez",
        "Foto": "assets/img/leandro-meza.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Luis Armando Tilve Salgado",
        "Foto": "assets/img/luis-tilve.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Luis Carlos Payares Joly",
        "Foto": "assets/img/luis-payares.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Maria De los Angeles Anaya Guardo",
        "Foto": "assets/img/maria-anaya.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Mauricio Andrés Arias Rodelo",
        "Foto": "assets/img/mauricio-ariaspng.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Óscar de Jesús Ballestas Ortega",
        "Foto": "assets/img/oscar-ballestas.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Ricardo Enrique Jaramillo Acevedo",
        "Foto": "assets/img/ricardo-jaramillo.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Ronald Paternina Castro",
        "Foto": "assets/img/ronald-paternina.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Steeven jose altamiranda gonzalez",
        "Foto": "assets/img/steeven-altamiranda.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Yilber Enrique Molina Devoz",
        "Foto": "assets/img/yilber-molina.jpg",
        "Cargo": "Profesional en Formacion"
    },
    {
        "Nombres": "Zuleydis Barrios Peña",
        "Foto": "assets/img/zuleydis-barrios.jpg",
        "Cargo": "Profesional en Formacion"
    }
]